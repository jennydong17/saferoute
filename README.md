### How to setup

0. Get binary dependencies for GDAL installed.  The easiest way to do this is to setup/use [Conda](http://conda.pydata.org/)
1. pip install -r requirements.txt

### How to run
1. cd into directory with manage.py
2. python manage.py makemigrations 
3. python manage.py migrate
4. python manage.py runserver
5. go to http://localhost:8000/
