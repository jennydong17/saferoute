import os
import datetime as dt

import numpy as np
import pandas as pd
from scipy.stats import gaussian_kde

# import json
# import seaborn as sns
# import matplotlib.pyplot as plt
# %matplotlib inline

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
CRIME_FNAME = BASE_DIR + "/saferoute/OPD_140215.csv"
CRIMEDEN_FNAME = BASE_DIR + "/saferoute/crime_density.csv" 
CRIME_CLEAN_FNAME = BASE_DIR + "/saferoute/crime_clean.csv"

# os.chdir("../../backend")

# raw = pd.read_csv("data/OPD_140215.csv", parse_dates=['Date'])
raw = pd.read_csv(CRIME_FNAME, parse_dates=['Date'])

clean = raw[(raw.Lat.notnull()) & (raw.Long.notnull())]

lower = 0.001
upper = 0.999
cols = ['Lat', 'Long']
for col in cols:
    keep = (clean[col] > clean[col].quantile(lower)) & (clean[col] < clean[col].quantile(upper))
    clean = clean[keep]

address_counts = clean.Addr.value_counts().to_frame().reset_index()
address_counts.columns = ['Addr', 'n_address']

clean = pd.merge(clean, address_counts, on='Addr')
clean = clean[clean.n_address < 10]

ctypes = ['PETTY THEFT', 'ROBBERY', 'NARCOTICS', 'FELONY ASSAULT', 'HOMICIDE', 'WEAPONS', 'THREATS',
          'FORCIBLE RAPE', 'KIDNAPPING', 'ATTEMPTED RAPE']

clean = clean[clean.CType.isin(ctypes)]

from_date = dt.date(2012, 1, 1)

data = clean
data = data[(data.Date >= from_date)]

n_points = 20

data_precalc = pd.read_csv(CRIMEDEN_FNAME)

filtered = data[(data.Date >= from_date)]

kde = gaussian_kde(filtered[['Lat', 'Long']].as_matrix().T)


def _heatmap(data, from_date, lat_bounds, long_bounds, n_points):

    x_support = np.linspace(lat_bounds[0], lat_bounds[1], n_points)
    y_support = np.linspace(long_bounds[0], long_bounds[1], n_points)
    xx, yy = np.meshgrid(x_support, y_support)


    z = kde([xx.ravel(), yy.ravel()]).reshape(xx.shape)

    return xx, yy, z

def get_heatmap_calc(min_lat, max_lat, min_long, max_long):

    x_points,y_points,z_points = _heatmap(clean,from_date,(min_lat,max_lat),(min_long,max_long),n_points)

    res = [(x,y,z) for x,y,z in zip(x_points.ravel(),y_points.ravel(),z_points.ravel())]

    return res


def get_heatmap(min_lat, max_lat, min_long, max_long):

    lat_bounds = (min_lat, max_lat)
    long_bounds = (min_long, max_long)

    filtered = data_precalc[(data_precalc.lat >= lat_bounds[0]) & (data_precalc.lat <= lat_bounds[1]) &
                (data_precalc.long >= long_bounds[0]) & (data_precalc.long <= long_bounds[1])]

    res = [tuple(x) for x in filtered.values]
    return res

clean_data_raw = pd.read_csv(CRIME_CLEAN_FNAME,parse_dates=['Date'])
from_date = dt.date(2012, 1, 1)
filtered_raw = clean_data_raw[(clean_data_raw.Date >= from_date)]
filtered_raw= filtered_raw[['Lat', 'Long']]

def get_heatmap_raw(min_lat, max_lat, min_long, max_long):

    res = [tuple(x)+(1,) for x in filtered_raw.values]
    return res


def get_danger(lat,long):

    z = kde([lat, long])

    return z

test_vals = [37.729725, 37.856966, -122.343620, -122.110161]

test_res = get_heatmap(test_vals[0], test_vals[1], test_vals[2], test_vals[3])
test_res = test_res[0:100]

for r in test_res:
    print r

print "---------------test calc--------------"

test_res = get_heatmap_calc(test_vals[0], test_vals[1], test_vals[2], test_vals[3])
test_res = test_res[0:100]

for r in test_res:
    print r

print get_danger(37.729725, -122.343620)
