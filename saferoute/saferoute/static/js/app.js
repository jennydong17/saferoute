var directionsDisplay;
var directionsService = new google.maps.DirectionsService();
var map;
var heatmap;
var geocoder;

var g = [
'rgba(255, 0, 0, 0)',
'rgba(255, 0, 0, 0.9)',
'rgba(255, 0, 0, 0.7)',
'rgba(255, 0, 0, 0.5)',
'rgba(255, 0, 0, 0.3)',
'rgba(255, 0, 0, 0.1)',
'rgba(255, 0, 0, 1)']

// var g = [
// 'rgba(255, 0, 0, 0)',
// 'rgba(255, 0, 0, 0.2)',
// 'rgba(255, 0, 0, 0.4)',
// 'rgba(255, 0, 0, 0.6)',
// 'rgba(255, 0, 0, 0.8)',
// 'rgba(255, 0, 0, 0.9)',
// 'rgba(255, 0, 0, 1)']

var overlays = [];
var markers = [];

function initialize() {
  geocoder = new google.maps.Geocoder();

  directionsDisplay = new google.maps.DirectionsRenderer( {
    suppressMarkers: true
  });

  var mapCanvas = document.getElementById('map');
  var mapOptions = {
    center: new google.maps.LatLng(37.8044, -122.2708),
    zoom: 10,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  }
  map = new google.maps.Map(mapCanvas, mapOptions);
  directionsDisplay.setMap(map);
}
  // var point = {
  //   location : "19th Bart Station, Oakland CA",
  //   stopover : false
  // }

  // var point1 = {
  //   location : "Frank H. Ogawa Plaza",
  //   stopover : false
  // }

  // var point2 = {
  //   location: new google.maps.LatLng(45.658197,-73.636333),
  //   stopover: false
  // }



function calcRoute() {
  // Remove any markers
   deleteMarkers();
   var start = document.getElementById("start").value;
   var end = document.getElementById("end").value;

  // var points=[point, point1]
  $.ajax({
    url: "get_points/?start=" + encodeURIComponent(start) +
     "&end=" + encodeURIComponent(end) +
     "&weight=" +  (dangermode ? "daredevil_dist" : "crime_dist")
  }).done(function (data) {
    if (console && console.log) {
      console.log("data:", data);
      var startLat = data.start;
      console.log("start:", start);

      var route_points = [];
      var arrayLength = data.points.length;
      for (var i = 0; i < arrayLength; i++) {
        var loc = {
          location: new google.maps.LatLng(data.points[i][0], data.points[i][1]),
          stopover: false
        }
        route_points.push(loc)
      }

      var request = {
        origin: start,
        destination: end,

        // origin:new google.maps.LatLng(37.7,-122.265739),
        // destination:new google.maps.LatLng(37.7259457624,-122.225825829),
        // waypoints: [{ location : start, stopover : false }, {location : end, stopover : false}],
        // waypoints: [{location: new google.maps.LatLng(37.7,-122.265739), stopover: false},{location: new google.maps.LatLng(37.7297580196,-122.249057028), stopover: false},{location: new google.maps.LatLng(37.7478365275,-122.241919083), stopover: false},{location: new google.maps.LatLng(37.7477633535,-122.251475877), stopover: false},{location: new google.maps.LatLng(37.7446069802,-122.258134378), stopover: false},{location: new google.maps.LatLng(37.7479390382,-122.235753774), stopover: false},{location: new google.maps.LatLng(37.7453718289,-122.226673407), stopover: false},{location: new google.maps.LatLng(37.7259457624,-122.225825829), stopover: false}],
        // waypoints: [{location: new google.maps.LatLng(37.7,-122.1), stopover: false},{location: new google.maps.LatLng(37.6993971738,-122.102679913), stopover: false},{location: new google.maps.LatLng(37.707073656,-122.105372432), stopover: false},{location: new google.maps.LatLng(37.7030105284,-122.096950044), stopover: false},{location: new google.maps.LatLng(37.6919787704,-122.089178511), stopover: false},{location: new google.maps.LatLng(37.6926462213,-122.090738383), stopover: false},{location: new google.maps.LatLng(37.6914677257,-122.09917435), stopover: false},{location: new google.maps.LatLng(37.6954987506,-122.103617178), stopover: false},{location: new google.maps.LatLng(37.7057201463,-122.129330174), stopover: false},{location: new google.maps.LatLng(37.7093283879,-122.131562024), stopover: false},{location: new google.maps.LatLng(37.7289484954,-122.148634619), stopover: false},{location: new google.maps.LatLng(37.7433573223,-122.157934341), stopover: false},{location: new google.maps.LatLng(37.7592522605,-122.169777635), stopover: false},{location: new google.maps.LatLng(37.7692467581,-122.180549805), stopover: false},{location: new google.maps.LatLng(37.7756823833,-122.185757817), stopover: false},{location: new google.maps.LatLng(37.7781053401,-122.187895035), stopover: false},{location: new google.maps.LatLng(37.7870303906,-122.194509865), stopover: false},{location: new google.maps.LatLng(37.7961326345,-122.203319078), stopover: false},{location: new google.maps.LatLng(37.800471,-122.254392), stopover: false},],
        // waypoints: [{location : "Frank H. Ogawa Plaza",
        //             stopover : false},
        //             {location : "19th Bart Station, Oakland CA",
        //             stopover : false},
        //             {location: new google.maps.LatLng(37.800471, -122.254392),
        //               stopover: false}
        //             ],
        waypoints: route_points,
        travelMode: google.maps.TravelMode.WALKING
      };

      //console.log("start:", start);
      //console.log("end:", end);
      //console.log("request:", request);
      //console.log("request origin:", request.origin);
      //console.log("request dest:", request.destination);

      //reset map i hope
      //directionsDisplay.setMap(null);

      deleteMarkers();

      directionsService.route(request, function(result, status) {
        if (status == google.maps.DirectionsStatus.OK) {
          directionsDisplay.setDirections(result);
          plotAddress(start);
          plotAddress(end);
        }
      });

    }
  });

}

// Retrieves heatmap crime data based on Map min/max lat long to display a heatmap
function calcHeatMap() {
  var latlngBounds = map.getBounds();
  var southwest = latlngBounds.getSouthWest();
  var northeast = latlngBounds.getNorthEast();

  $.ajax({
    url: "get_heatmap_points/?minLat=" + encodeURIComponent(southwest.lat()) +
    "&minLong=" + encodeURIComponent(southwest.lng()) +
    "&maxLat=" + encodeURIComponent(northeast.lat()) +
    "&maxLong=" + encodeURIComponent(northeast.lng())
  }).done(function (data) {
    var heatmap_data = [];
    for (var i = 0; i < data.points.length; i++) {
      var point = {
        location: new google.maps.LatLng(data.points[i][0], data.points[i][1]),
        weight: data.points[i][2]
      }
      heatmap_data.push(point)
    }

    heatmap = new google.maps.visualization.HeatmapLayer({
      data: heatmap_data,
      dissipating: true,
      radius: 30
    });

    heatmap.setMap(map);
  })
}

// Clears Map
function clearAll() {
  clearHeatMap();
  directionsDisplay.setMap(null);
  directionsDisplay = new google.maps.DirectionsRenderer( {
    suppressMarkers: true
  });
  directionsDisplay.setMap(map);
  deleteMarkers();
}

// Sets the map on all markers in the array.
function setMapOnAll(map) {
  for (var i = 0; i < markers.length; i++) {
    markers[i].setMap(map);
  }
}

// Removes the markers from the map, but keeps them in the array.
function clearMarkers() {
  setMapOnAll(null);
}

// Deletes all markers in the array by removing references to them.
function deleteMarkers() {
  clearMarkers();
  markers = [];
}

// Removes heatmap
function clearHeatMap() {
  if (heatmap) {
    heatmap.setMap(null);
    heatmap.getData().j = [];
    heatmap.setMap(map);
  };
}

// Transforms address into LatLong and plots on Map
function plotAddress(address) {
  geocoder.geocode( { 'address': address}, function(results, status) {
    if (status == google.maps.GeocoderStatus.OK) {
      map.setCenter(results[0].geometry.location);
      var marker = new google.maps.Marker({
          map: map,
          position: results[0].geometry.location
      });
      markers.push(marker)
    } else {
      alert("Geocode was not successful for the following reason: " + status);
    }
  });
}

google.maps.event.addDomListener(window, 'load', initialize);
