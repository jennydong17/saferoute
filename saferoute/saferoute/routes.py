import networkx as nx
import json
import numpy as np
import itertools
import os
from geopy.geocoders import Nominatim;
import math
import os.path
import cPickle as pickle

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

def addressToGeo(start, end) :
	geolocator = Nominatim()
	startLoc = geolocator.geocode(start)
	endLoc = geolocator.geocode(end)
	return (startLoc.latitude, startLoc.longitude, endLoc.latitude, endLoc.longitude)

def get_path(n0, n1):
	"""If n0 and n1 are connected nodes in the graph, this function
	return an array of point coordinates along the road linking
	these two nodes."""
	return np.array(json.loads(sg[n0][n1]['Json'])['coordinates'])

def geocalc(lat0, lon0, lat1, lon1):
	earth_r = 6372.8
	lat0 = np.radians(lat0)
	lon0 = np.radians(lon0)
	lat1 = np.radians(lat1)
	lon1 = np.radians(lon1)
	dlon = lon0 - lon1
	y = np.sqrt(
		(np.cos(lat1) * np.sin(dlon)) ** 2
		 + (np.cos(lat0) * np.sin(lat1)
		  - np.sin(lat0) * np.cos(lat1) * np.cos(dlon)) ** 2)
	x = np.sin(lat0) * np.sin(lat1) + \
	np.cos(lat0) * np.cos(lat1) * np.cos(dlon)
	c = np.arctan2(y, x)
	return earth_r * c

def get_path_length(path):
	return np.sum(geocalc(path[1:,0], path[1:,1],
						  path[:-1,0], path[:-1,1]))

#SHP_FNAME = BASE_DIR + "/saferoute/san-francisco-bay_california_osm_roads.shp"
SHP_FNAME = BASE_DIR + "/saferoute/streets.shp"


CACHE = {}

#if 'graph' not in CACHE:
if True:
	#if os.path.exists(BASE_DIR + "/saferoute/graph.pkl"):
	if False:
		print "Reading graph cache " + BASE_DIR + "/saferoute/graph.pkl"
		sg = pickle.load(open(BASE_DIR + "/saferoute/graph.pkl", "rb" ))
	else:
		print "Reading shape file..."
		print SHP_FNAME
		g = nx.read_shp(SHP_FNAME)
		print "Getting biggest connected subgraph..."
		sgs = list(nx.connected_component_subgraphs(g.to_undirected()))
		sgs.sort(key=len)
		sg = sgs[-1]


		dat = np.loadtxt(BASE_DIR + '/saferoute/crime2.csv', delimiter=',')
		misses = 0
		for (lon0, lat0, lon1, lat1, crime) in dat:

			try:
				sg.edge[(lon0, lat0)][(lon1, lat1)]['crime'] = crime
			except:
				misses += 1
				pass
		print "There were " + str(misses) + "unknown locs."
		for n0, n1 in sg.edges_iter():
			path = get_path(n0, n1)
			distance = get_path_length(path)
			sg.edge[n0][n1]['distance'] = distance
			sg.edge[n0][n1]['crime_dist'] = distance * sg[n0][n1].get('crime', 0.0)
			sg.edge[n0][n1]['daredevil_dist'] = distance / (sg[n0][n1].get('crime', 0.0))# / (1.0 + 50*sg[n0][n1].get('crime', 0.0))


		print "Writing cache " + BASE_DIR + "/saferoute/graph.pkl"
		pickle.dump(sg, open(BASE_DIR + "/saferoute/graph.pkl", "wb" ) )

	CACHE['graph'] = sg

sg = CACHE['graph']



# g = nx.read_shp(SHP_FNAME)
#


nodes = np.array(sg.nodes())
# # Get the closest nodes in the graph.

# # Compute the length of the road segments.
# print "Computing road distances..."



def hardcoded_find_way_points(start="", end=""):
	return route_to_json(edges_to_lat_long(*get_route()))
	# s = "[{location: new google.maps.LatLng(37.7,-122.265739), stopover: false},{location: new google.maps.LatLng(37.7297580196,-122.249057028), stopover: false},{location: new google.maps.LatLng(37.7478365275,-122.241919083), stopover: false},{location: new google.maps.LatLng(37.7477633535,-122.251475877), stopover: false},{location: new google.maps.LatLng(37.7446069802,-122.258134378), stopover: false},{location: new google.maps.LatLng(37.7479390382,-122.235753774), stopover: false},{location: new google.maps.LatLng(37.7453718289,-122.226673407), stopover: false},{location: new google.maps.LatLng(37.7259457624,-122.225825829), stopover: false},{location: new google.maps.LatLng(37.7183804243,-122.202931834), stopover: false},{location: new google.maps.LatLng(37.7184138681,-122.203593166), stopover: false},{location: new google.maps.LatLng(37.7168449434,-122.20749117), stopover: false},{location: new google.maps.LatLng(37.7196105524,-122.202206548), stopover: false},{location: new google.maps.LatLng(37.7323361254,-122.198847668), stopover: false},{location: new google.maps.LatLng(37.7415684565,-122.196415658), stopover: false},{location: new google.maps.LatLng(37.7577007702,-122.188396524), stopover: false},{location: new google.maps.LatLng(37.7684298578,-122.173548569), stopover: false},{location: new google.maps.LatLng(37.7759952798,-122.184896325), stopover: false},{location: new google.maps.LatLng(37.7779490176,-122.188010621), stopover: false},{location: new google.maps.LatLng(37.7896273556,-122.196861408), stopover: false},{location: new google.maps.LatLng(37.800925742,-122.219061465), stopover: false},{location: new google.maps.LatLng(37.802357874,-122.230179222), stopover: false},{location: new google.maps.LatLng(37.8015649459,-122.24577568), stopover: false},{location: new google.maps.LatLng(37.800471,-122.254392), stopover: false},]"
	# s = "[{location: new google.maps.LatLng(37.7259457624,-122.225825829), stopover: false},{location: new google.maps.LatLng(37.7183804243,-122.202931834), stopover: false},{location: new google.maps.LatLng(37.7184138681,-122.203593166), stopover: false},{location: new google.maps.LatLng(37.7168449434,-122.20749117), stopover: false},{location: new google.maps.LatLng(37.7196105524,-122.202206548), stopover: false},{location: new google.maps.LatLng(37.7323361254,-122.198847668), stopover: false}]"
	# return """ [{
	#	 location : "Frank H. Ogawa Plaza",
	#	 stopover : false
	#   }, {
	#	 location : "19th Bart Station, Oakland CA",
	#	 stopover : false
 #  	}, {location: new google.maps.LatLng(37.7,-122.265739)}]"""
 	# return s

def get_route(pos0=(37.7, -122.1), pos1=(37.800471, -122.254392), weight='distance'):
	return edges_to_lat_long(*get_edges(pos0, pos1, weight))

def get_edges(pos0, pos1, weight):
	pos0_i = np.argmin(np.sum((nodes[:,::-1] - pos0)**2, axis=1))
	pos1_i = np.argmin(np.sum((nodes[:,::-1] - pos1)**2, axis=1))

	# Compute the shortest path.
	print "Getting route based on " + weight
	path = nx.shortest_path(sg,
							source=tuple(nodes[pos0_i]),
							target=tuple(nodes[pos1_i]),
							weight=weight)

	edges = [sg.edge[path[i]][path[i + 1]] for i in range(len(path) - 1)]

	return (pos0, pos1, edges)

def get_edge_coords(edge, start):
	coords = json.loads(edge['Json'])['coordinates']
	coords = [c[::-1] for c in coords]
	d0 = geocalc(start[0], start[1], coords[0][0], coords[0][1])
	d1 = geocalc(start[0], start[1], coords[-1][0], coords[-1][1])

	return coords
	# if d0 < d1:
	# 	return coords
	# else:
	# 	return coords[::-1]


def get_lat_longs(edges, start):
	lat_longs = []
	for i in range(len(edges)):
		lat_longs.extend(get_edge_coords(edges[i], start))
		start = lat_longs[-1]

	return lat_longs


def edges_to_lat_long(pos0, pos1, edges):
	lat_longs = get_lat_longs(edges, pos0)
	# lat_longs = edges
	if len(lat_longs) > 6:
		idxs = [int(math.ceil(i)) for i in np.linspace(0, len(lat_longs), 8)][1:-1]
		lat_longs = [lat_longs[i] for i in idxs]
	return (list([pos0]) + lat_longs + list([pos1]))

			# return lat_longs[:20]
	# [37.810183, -122.265739, [-122.26355302649799, 37.81108234553244], [-122.26231418120895, 37.81088587372204], [-122.26238475683368, 37.81034356458673], [-122.26218275296719, 37.81062025121048], [-122.2615861290993, 37.81076869471568], [-122.26314122359508, 37.8066283698241], [-122.26335554885924, 37.80747879772008], [-122.26332872676907, 37.80788322454822], [-122.26277946065412, 37.80607038652985], [-122.26180581878148, 37.80281284368047], [-122.26114314551657, 37.80377793601187], [-122.26092437784375, 37.80414129151447], [-122.26117080579705, 37.80449081687678], [-122.26198670025195, 37.80504184319142], [-122.26171462367495, 37.79916294394357], [-122.26232021617923, 37.80077486774289], [-122.25992936211802, 37.79829784771704], [-122.26139435115469, 37.79869934087904], [-122.25992936211802, 37.79829784771704], [-122.2581961521798, 37.798273959293], [-122.25624367165457, 37.79935472188819], [-122.25509350690109, 37.8000248550469], 37.800471, -122.254392]
def route_to_json(lat_longs):
	s = "["
	for lat_long in lat_longs:
	# 37.7183804243,-122.202931834
		# if (lat_long[0] !=37.7183804243 && lat_long[1]!=-122.202931834):
		s = s + '{location: new google.maps.LatLng(' + str(lat_long[0]) + ',' + str(lat_long[1]) + '), stopover: false},'
	s = s + "]"
	return s
