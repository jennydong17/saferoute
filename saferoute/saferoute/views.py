from django.shortcuts import render
from django.http import JsonResponse
from django.core.exceptions import SuspiciousOperation

import routes
import heatmap

def index(request):
	points_list = routes.get_route((37, -122), (38, -122))
	# import pdb
	# pdb.set_trace()
	start = "new google.maps.LatLng(" + str(points_list[0][0]) + "," + str(points_list[0][1]) + ")"
	end = "new google.maps.LatLng" + "(" + str(points_list[-1][0]) + "," + str(points_list[-1][1]) + ")"
	if len(points_list) > 8:
		points_list = points_list[1:7]
	else:
		points_list = points_list[1:-1]
	points = routes.route_to_json(points_list)
	return render(request, 'index.html', {'points' : points,
		'start':start, 'end':end})
def daredevil(request, start, end):
	return render(request, 'danger_mode.html', {'start':start, 'end':end})
def geo(request):
    start = request.GET.get('start', None)
    end = request.GET.get('end', None)
    if start is None or end is None:
        raise SuspiciousOperation("need start and end query parameters")

    (slat, slong, elat, elong) = routes.addressToGeo(start, end)

    json = {'start_lat': slat,
            'start_long': slong,
            'end_lat': elat,
            'end_long': elong}
    return JsonResponse(json)

def get_points(request):
    start = request.GET.get('start', None)
    end = request.GET.get('end', None)
    weight = request.GET.get('weight', 'distance')
    if start is None or end is None:
        raise SuspiciousOperation("need start and end query parameters")

    (slat, slong, elat, elong) = routes.addressToGeo(start, end)
    points_list = routes.get_route((slat, slong), (elat, elong), weight)
    start = points_list[0]
    end = points_list[-1]
    if len(points_list) > 8:
        points_list = points_list[1:7]
    else:
        points_list = points_list[1:-1]

    json = {'points' : points_list,
            'start' : start,
            'end' : end}
    return JsonResponse(json)

def get_heatmap_points(request):
    minLat =  request.GET.get('minLat', None)
    minLong = request.GET.get('minLong', None)
    maxLat =  request.GET.get('maxLat', None)
    maxLong = request.GET.get('maxLong', None)

    if minLat is None or minLong is None or maxLat is None or maxLong is None:
        raise SuspiciousOperation("Need all parameters")

    heatmap_points = heatmap.get_heatmap_raw(float(minLat), float(maxLat), float(minLong), float(maxLong))

    json = {'points' : heatmap_points}
    return JsonResponse(json)
